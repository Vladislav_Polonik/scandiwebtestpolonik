<div class="head">
    <h1>Product list</h1>
    <div class="buttons">
        <a href="add" class="button">add</a>
        <a href="delete" class="button">mass delete</a>
    </div>
</div>
<hr>
<div class="main">
    <div class="products">
        <?php foreach ($products as $product): ?>
            <div class="product">
                <input type="checkbox" name="<?= $product['id'] ?>">
                <p><?= $product['sku'] ?></p>
                <p><?= $product['name'] ?></p>
                <p><?= $product['price'] ?></p>
                <p><?= $product['attribute'] . ': ' . $product['value'] . ' ' . $product['measure'] ?></p>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php
