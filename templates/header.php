<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta name="author" content="Vladislav Polonik">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <title><?= $title ?: 'Test task'?>></title>
</head>
<body>


