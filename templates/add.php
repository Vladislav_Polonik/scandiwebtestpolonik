<div class="head">
    <h1>Product Add</h1>
    <div class="buttons">
        <a href="save" class="button">Save</a>
        <a href="/" class="button">Cancel</a>
    </div>
</div>
<hr>
<div class="main">
    <form class="new_product" name="new_product" action="/product/new" method="post" id="new_product">
        <label>SKU<input name="sku" type="text" required></label>
        <label>Name<input name="name" type="text" required></label>
        <label>Price ($)<input name="price" type="text" required></label>
        <label>Type Switcher
            <select name="type" required id="type_switcher">
                <option>DVD-disc</option>
                <option>Book</option>
                <option>Furniture</option>
            </select>
        </label>
        <p class="for_dvd variable">Please, provide dimensions</p>
        <label class="for_dvd variable">Size (MB)<input name="size" type="text"></label>
        <p class="for_furniture variable hidden">Please, provide size</p>
        <label class="for_furniture variable hidden">Height (CM)<input name="height" type="text"></label>
        <label class="for_furniture variable hidden">Width (CM)<input name="width" type="text"></label>
        <label class="for_furniture variable hidden">Length (CM)<input name="length" type="text"></label>
        <p class="for_book variable hidden">Please, provide weight</p>
        <label class="for_book variable hidden">Weight (KG)<input name="weight" type="text"></label>

</div>

<?php
