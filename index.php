<?php
define('ROOT', $_SERVER['DOCUMENT_ROOT']);

require_once ROOT . '/classes/Router.php';

$pages = (require ROOT . '/config.php')['pages'];
$router = new Router($pages);
$router->start($_REQUEST);

