let select = document.getElementById('type_switcher');

select.onchange = function (){

    let allElements = document.getElementsByClassName("variable");
    for(let item of allElements){
        item.classList.add('hidden');
    }

    let visible;
    switch (select.value){
        case 'DVD-disc':
            visible = document.getElementsByClassName('for_dvd');
            break;
        case 'Book':
            visible = document.getElementsByClassName('for_book');
            break;
        case 'Furniture':
            visible = document.getElementsByClassName('for_furniture');
            break;
    }
    for(let item of visible){
        item.classList.remove('hidden');
    }
}