<?php


class Db_connect
{
    public static function getConnect()
    {
        $connectData = (require ROOT . '/config.php')['db'];

        $hostname = $connectData['hostname'];
        $database = $connectData['database'];
        $port = $connectData['port'];
        $username = $connectData['username'];
        $password = $connectData['password'];

        $dbh = new PDO("mysql:host=$hostname;dbname=$database;port=$port", $username, $password);

        return $dbh;
    }
}