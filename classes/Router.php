<?php

require ROOT . '/classes/Products.php';

class Router
{
    protected static $pagesList = [];

    public function start($data)
    {

        $products = Products::getProducts();

        require_once ROOT . '/templates/header.php';

        require_once ROOT . '/templates/products.php';

//        require_once ROOT . '/templates/add.php';

        require_once ROOT . '/templates/footer.php';

    }
        public function __construct($pages)
    {
        foreach ($pages as $page){
            self::$pagesList[] = $page;
        }
    }

}



