<?php

require_once ROOT . '/classes/Db_connect.php';
class Products
{
    public static function getProducts()
    {
        $query = 'SELECT Prod.id, sku, Prod.name AS name, CONCAT(CAST(price AS CHAR(15)), " $") AS "price", attribute, `value`, measure FROM products Prod LEFT JOIN ( SELECT products_id, IF(COUNT(attributes.name) > 1, "Dimension", MAX(attributes.name)) AS attribute, GROUP_CONCAT(`value` SEPARATOR "x") AS "value", MAX(measure) AS "measure" FROM properties JOIN attributes ON properties.attributes_id = attributes.id GROUP BY products_id ) Prop ON Prod.id = Prop.products_id';

        $dbh = Db_connect::getConnect();
        $stmt = $dbh->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}